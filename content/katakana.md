---
title: "Katakana"
date: 2021-10-22T17:15:48-03:00
draft: false
parameter1: images/alfabeto_katakana.png
parameter2: images/Dakuten_Handakuten_katakana.jpg
parameter3: images/katakana-juncoes.png
parameter4: images/sinal_de_prolongamento.png
---

# Katakana (カタカナ) <h1>

O Katakana é outro sistema básico de escrita e todos sons que existem no Hiragana existem no Katakana. No entanto, sua utilização é bem mais restrita, sendo usado para palavras de origem estrangeira e onomatopeias.

No fim da página é disponibilizado uma tabela com o alfabeto completo de Katakana.

# Dakuten (濁点)	e	Handakuten (半濁点) <h2>

Da mesma forma que no Hiragana, os sinais  ゛ e  ゜podem ser utilizados para produção de novos sons no Katakana. Tais letras são modificadas e apresentadas no final da página.

# Junção de palavras

A junção de palavras também ocorre no Katakana e o princípio é o mesmo do Hiragana, inclusive com as mesmas restrições conforme mostrado no fim da página.

# ー　- sinal de prolongamento

O sinal de prolongamento "ー" tem a função de prolongar a sílaba anterior produzindo um som mais elongado. É interessante a existência de palavras cujo significado difere apenas do som prolongado. A aplicação desse sinal de prolongamento é exibido no final da página.

