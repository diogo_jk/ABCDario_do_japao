---
title: "_Index"
date: 2021-10-22T17:15:14-03:00
draft: false
---
# Um breve resumo <h1>
A língua japonesa surgiu a milhares de ano com uma história um tanto quanto desconhecida - pouco se sabe sobre a pré-história da língua japonesa, com textos formais aparecendo somente em meados do século VIII [1]. Apesar de suas origens ignotas, a língua japonesa moderna é o extremo oposto do desconhecido, sendo a nona língua mais falada no mundo e aparecendo em diversas partes da cultura POP mundial em músicas, animações, jogos, mangás, etc. 

# Como funciona a língua japonesa? <h1>
A escrita japonesa possui três sistemas	diferentes: Hiragana（ひらがな) | Katakana （カタカナ）| Kanji（漢字）.	Sendo todos eles amplamente utilizados no cotidiano japônes, em geral a estrutura dos dois primeiros alfabetos (Hiragana e Katakana) se baseia em sílabas, ou seja, não existem letras "soltas" no alfabeto japônes (como por exemplo "p", "d", etc.) e todas consonantes - ou melhor, quase todas - são acompanhadas de uma vogal para produzir um silábario fonético. Já no terceiro alfabeto, Kanji, cada símbolo representa um significado e a junção de símbolos não necessariamente é a soma dos dois - é fácil dizer que aqui se concentrará mais da metade do seu estudo e também aonde a expressividade da língua japonesa é realmente mostrada. 
Agora, para iniciarmos essa jornada, venha comigo para uma introdução do primeiro alfabeto japônes, o Hiragana.