---
title: "Hiragana"
date: 2021-10-22T17:15:35-03:00
draft: false
parameter1: images/Hiragana-table-1024x710.jpg
parameter2: /images/Dakuten_Handakuten.jpg
parameter3: /images/Juncao.jpg
parameter4: /images/tsu_pequeno.jpg
---

# Hiragana (ひらがな)　<h1>

O Hiragana é o sistema mais básico da escrita japonesa e também o primeiro que se aprende no estudo da mesma. Com ele é possível escrever _**todas**_ as palavras em japônes mas isso não se é feito e _**não deve ser feito**_ na prática, e pouco se sabe o porquê - é mais um caso de costume, dentro de uma frase o japônes misturará alfabetos com uma certa lógica, o Hiragana será usado para partículas, conjungação de verbos, adjetivação, flexões e palavras que não possuam ou que seja muito complicado escrever seu Kanji, o Katakana é usado para onomatopeias e nomes estrangeiros e o Kanji para quase todo o resto.

O Hiragana dá fundamento para todos os alfabetos e deve ser o ponto de partida, no fim da página temos uma tabela que mostra todo o alfabeto Hiragana e, não tem jeito, ela _**deve ser decorada**_.


# Dakuten (濁点)	e	Handakuten (半濁点) <h2>

Existem dois sinais na língua japonesa que serve o propósito de modificar o som de algumas letras. Esses sinais funcionam tanto para o Hiragana quanto o Katakana. o Dakuten se refere ao sinal ゛e o Handakuten ao sinal ゜, as letras são modificadas e mostrada no final da página.


# Junção de letras

Ainda mais, podemos aglutinar duas letras para produzir novos sons. Nesse caso, a segunda letra é escrita numa proporção de 1/4 da primeira e há um número restrito de palavras em que pode ser feito isso, conforme a imagem no fim da página.

# つ pequeno

Também temos a ferramenta de pausa, quando adicionamos o tsu（つ）pequeno é como se paussásemos antes de pronunciar a próxima letra. O resultado escrito é a consoante duplicada como mostrar a figura no fim da página.