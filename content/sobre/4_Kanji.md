---
title: "Kanjis Favoritos"
date: 2021-10-22T17:25:38-03:00
draft: false
parameter1: /images/Amor_Kanji.png
parameter2: /images/Água.png
parameter3: /images/Estudo_Kanji.png
parameter4: /images/Luz_kanji.png
---
Abaixo estão o que considero os Kanjis mais interessantes da língua japonesa - juntos de sua leitura em Hiragana e sua tradução, escritos por mim :