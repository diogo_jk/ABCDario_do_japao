---
title: "Familiares Katakana"
date: 2021-10-22T17:39:25-03:00
draft: false
parameter1: /images/Diogo_Katakana.png
parameter2: /images/Paula_katakana.png
parameter3: /images/Eduardo_katakana.png
parameter4: /images/Rafael_katakana.png
---

Abaixo estão os nomes da minha família em Katakana escritos por mim, junto com o sobrenome japônes de nossa família, Koike (小池):