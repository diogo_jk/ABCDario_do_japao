---
title: "Kanji"
date: 2021-10-22T17:15:43-03:00
draft: false
parameter1: /images/Kanji_1.jpg
parameter2: /images/Jukugo.png
parameter3: /images/Radicais.png
parameter4: /images/Estrutura.png
---

# Kanji (漢字)

O Kanji é facilmente o alfabeto mais difícil dentre os 3, e essa dificuldade origina da quantidade de ideogramas e das inúmeras combinações que surgem das junções dos mesmos; apesar da dificuldade, é no Kanji que surge - na minha opinião - a arte da língua japonesa, cada ideograma representa uma beleza própria e também seu próprio significado, elevando ao nível artístico o simples ato de escrever. 

É claro que, como também para os outros alfabetos aqui descritos, essa pequena introdução não bastará para ensinar a imensidão do Kanji. Para se ter noção da grandeza de ideogramas, é esperado que - na sociedade japonesa - um adulto funcional aprenda mais de 2000 ideogramas para uso diário, uma tarefa bastante árdua até para os nativos. Na imagem no fim da página são apresentados os primeiros 80 Kanjis ensinados a um aluno do primeiro ano do ensino fundamental.

# Jukugo (熟語)

Jukugo são palavras compostas por mais de um Kanji. Como vimos anteriormente, não juntamos Kanjis a fim de produzir um som e sim um significado. São exemplificados no fim da páginas Jukugos mais conhecidos.

# Como são formados os Kanjis? 

Se você já observou os Kanjis detalhadamente, deve ter percebido que várias partes se repetem em vários ideogramas diferentes, essas partes fundamentais são conhecidas como radicais. Conforme você ficar mais familiarizado com os ideogramas, você  passará a prestar atenção nos elementos que os formam, e não mais em traços isolados. O conhecimento desses elementos lhe ajuda a lembrar do kanji, assim como permite fazer associações para lembrar de seu significado. No final da página são mostrados os principais radicais dos Kanjis e como os mesmos são estruturados dentro de um kanji.


